#ifndef PERSONAJE_H
#define PERSONAJE_H


class Personaje
{
    public:
        Personaje();
        virtual ~Personaje();
        void avanzar();
        void retroceder();


    protected:

    private:
};

#endif // PERSONAJE_H
